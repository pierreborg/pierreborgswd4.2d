-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2019 at 08:01 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `BookingID` int(11) NOT NULL,
  `TimeSlotID` int(11) NOT NULL,
  `Guests` int(11) NOT NULL,
  `Comments` varchar(255) NOT NULL,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TableID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `BookingDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`BookingID`, `TimeSlotID`, `Guests`, `Comments`, `Created`, `Updated`, `TableID`, `UserID`, `BookingDate`) VALUES
(49, 2, 12, '12', '2019-05-19 18:49:52', '2019-05-19 18:49:52', 2, 3, '2019-05-17'),
(52, 4, 1, '21', '2019-05-20 14:07:50', '2019-05-20 14:07:50', 3, 9, '2019-05-21'),
(53, 5, 21, '12', '2019-05-20 14:08:04', '2019-05-20 14:08:04', 3, 9, '2019-05-21'),
(54, 2, 2, '26', '2019-05-20 14:08:53', '2019-05-20 14:08:53', 7, 9, '2019-05-23'),
(55, 2, 15, 'Next to terrace please', '2019-05-20 19:33:24', '2019-05-20 19:33:24', 1, 9, '2019-05-31');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `ImageID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`ImageID`, `Name`, `Location`) VALUES
(1, 'img1', 'img/img1.jpg\r\n'),
(2, 'img2', 'img/img2.jpg'),
(3, 'img3', 'img/img3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `PermissionID` int(2) NOT NULL,
  `Title` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`PermissionID`, `Title`) VALUES
(1, 'a'),
(2, 'u');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_tbl`
--

CREATE TABLE `restaurant_tbl` (
  `Restaurant_ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `TownID` int(25) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Tables` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant_tbl`
--

INSERT INTO `restaurant_tbl` (`Restaurant_ID`, `Name`, `TownID`, `PhoneNumber`, `Email`, `Tables`) VALUES
(1, 'Ta\' Borg Restaurant', 1, '21234567', 'Borg@restaurantapp.com', 50);

-- --------------------------------------------------------

--
-- Table structure for table `specials`
--

CREATE TABLE `specials` (
  `ID` int(11) NOT NULL,
  `Specials` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specials`
--

INSERT INTO `specials` (`ID`, `Specials`) VALUES
(1, 'Tomahawk Rib-eye'),
(4, 'Garganelli'),
(5, 'Pizza Salmon'),
(15, 'Beef Salad');

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `TableID` int(11) NOT NULL,
  `Seating` int(2) NOT NULL,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`TableID`, `Seating`, `Created`, `Updated`) VALUES
(1, 2, '2019-05-15 03:25:35', '2019-05-15 03:25:35'),
(2, 4, '2019-05-15 03:25:35', '2019-05-15 03:25:35'),
(3, 4, '2019-05-15 03:25:35', '2019-05-15 03:25:35'),
(4, 6, '2019-05-15 03:25:35', '2019-05-15 03:25:35'),
(5, 6, '2019-05-15 03:25:35', '2019-05-15 03:25:35'),
(6, 12, '2019-05-15 03:25:57', '2019-05-15 03:25:57'),
(7, 12, '2019-05-15 03:25:57', '2019-05-15 03:25:57'),
(8, 2, '2019-05-15 03:25:57', '2019-05-15 03:25:57'),
(9, 2, '2019-05-15 03:25:57', '2019-05-15 03:25:57');

-- --------------------------------------------------------

--
-- Table structure for table `timeslot`
--

CREATE TABLE `timeslot` (
  `TimeSlotID` int(11) NOT NULL,
  `timeslot` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeslot`
--

INSERT INTO `timeslot` (`TimeSlotID`, `timeslot`) VALUES
(1, '12:00-14:00'),
(2, '14:00-16:00'),
(3, '16:00-18:00'),
(4, '18:00-20:00'),
(5, '20:00-22:00');

-- --------------------------------------------------------

--
-- Table structure for table `town`
--

CREATE TABLE `town` (
  `TownID` int(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Street` varchar(255) NOT NULL,
  `Postcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `town`
--

INSERT INTO `town` (`TownID`, `Name`, `Street`, `Postcode`) VALUES
(1, 'Birkirkara', 'Triq Lewis V.Farrugia', 'BKR 1113');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(50) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Title` char(1) NOT NULL DEFAULT 'u',
  `User_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Surname` varchar(50) NOT NULL,
  `Update_user` datetime NOT NULL,
  `ConfirmedEmail` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Username`, `Password`, `Email`, `Title`, `User_created`, `Surname`, `Update_user`, `ConfirmedEmail`) VALUES
(1, 'Owner', 'd882b8721a224d38ebb54559e6b54e5df3a1bc6d', 'Borg@restaurantapp.com', 'a', '2019-05-13 10:05:24', 'Borg', '2019-05-15 01:47:50', 1),
(3, 'Johnattard', 'b2fe04a911d159fc41d57f2ece26ecbfab85bf01', 'jattard@gmail.com', 'u', '2019-05-19 16:49:33', 'attard', '0000-00-00 00:00:00', 1),
(9, 'PierreBorg', 'ff019a5748a52b5641624af88a54a2f0e46a9fb5', 'pierreborg1411@gmail.com', 'u', '2019-05-13 11:15:00', 'Borg', '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`BookingID`),
  ADD KEY `TimeSlotID` (`TimeSlotID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `TableID` (`TableID`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`ImageID`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`PermissionID`),
  ADD KEY `Title` (`Title`);

--
-- Indexes for table `restaurant_tbl`
--
ALTER TABLE `restaurant_tbl`
  ADD PRIMARY KEY (`Restaurant_ID`),
  ADD KEY `TownID` (`TownID`);

--
-- Indexes for table `specials`
--
ALTER TABLE `specials`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`TableID`);

--
-- Indexes for table `timeslot`
--
ALTER TABLE `timeslot`
  ADD PRIMARY KEY (`TimeSlotID`);

--
-- Indexes for table `town`
--
ALTER TABLE `town`
  ADD PRIMARY KEY (`TownID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `Title` (`Title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `BookingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `ImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `PermissionID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `restaurant_tbl`
--
ALTER TABLE `restaurant_tbl`
  MODIFY `Restaurant_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `specials`
--
ALTER TABLE `specials`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `TableID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `timeslot`
--
ALTER TABLE `timeslot`
  MODIFY `TimeSlotID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `town`
--
ALTER TABLE `town`
  MODIFY `TownID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`TimeSlotID`) REFERENCES `timeslot` (`TimeSlotID`),
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `booking_ibfk_3` FOREIGN KEY (`TableID`) REFERENCES `tables` (`TableID`);

--
-- Constraints for table `restaurant_tbl`
--
ALTER TABLE `restaurant_tbl`
  ADD CONSTRAINT `restaurant_tbl_ibfk_1` FOREIGN KEY (`TownID`) REFERENCES `town` (`TownID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`Title`) REFERENCES `permissions` (`Title`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
