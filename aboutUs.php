<?php 
session_start();
if(!isset($_SESSION['Username'])){
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--FontAwesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Ta' Borg Restaurant</title>
</head>

<body class="bg-info">

    <div class="container-fluid bg-warning">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">Ta' Borg Restaurant</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="aboutUs.php"><i class="fas fa-info-circle"></i> About Us<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="menu.php"><i class="fas fa-file-alt"></i> View Menu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactUs.php"><i class="fas fa-envelope"></i> Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#registerModal"><i class="fas fa-user-plus"></i> Register</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <div class="container">
        <div class="col-md-12">
            <h2 class="text-center">About Us</h2>
            <p>Ta' Borg Restaurant offers an enjoyable dining experience where you can come with your family and friends and enjoy our delicous food.</p>
            <h3><u>Menu Information:</u></h3>
            <p>The restaurant offers a vast menu with different types of food, ranging from starters, such as Calamari Fritti, to main course dishes, such as Tagliata di Manzo (cooked to your liking). The food offered at our restaurant is catered to your particular needs. By this, we mean that if you are Gluten intolerant, lactose intolerant, or vegan, the staff will guide you in choosing a plate according to your requirements.</p>
            <h3><u>Kids Play Area:</u></h3>
            <p>In our restaurant, there is also a sizeable Kids Play Area, where you can be rest-assured that your children are enjoying themselves and kept quiet.</p>
            <h3><u>What do I need to do if I want to book a table at your restaurant?</u></h3>
            <p>Firstly, if you don't have an account, click on "Register" to register for an account. When you have successfully registered to our website, you can fill in the form that will ask you the date and time you want to reserve a table, how many people will you be and lastly, if you have any other comments you would like to add to your booking.</p>
            <h3><u>Where is our restaurant located at?</u></h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d285.6941151117701!2d14.468346761645961!3d35.89164888615832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smt!4v1540889659631" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <!-- Register Modal -->
        <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="registerLabel">Register for an account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="adduser.php">

                            <input type="text" id="fName" name="firstname" placeholder="First Name" class="form-control" /><br />

                            <input type="text" id="lName" name="lastname" placeholder="Last Name" class="form-control" /><br />

                            <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                            <input type="text" placeholder="Password" class="form-control" name="password"><br>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <input type="submit" class="btn btn-primary" value="Register">
                    </div>
                </div>
            </div>
        </div>

        <!-- Login Modal -->
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginLabel">Login to your account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="viewuser.php">
                            <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                            <input type="text" placeholder="Password" class="form-control" name="password"><br>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <input type="submit" class="btn btn-secondary" value="Login">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
<?php
}
else{
    
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--FontAwesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Ta' Borg Restaurant</title>
</head>

<body class="bg-info">

    <div class="container-fluid bg-warning">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">Ta' Borg Restaurant</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="aboutUs.php"><i class="fas fa-info-circle"></i> About Us<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="menu.php"><i class="fas fa-file-alt"></i> View Menu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactUs.php"><i class="fas fa-envelope"></i> Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="Logout.php" class="nav-link" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> Logout</a>
                    </li>
                    <li class="nav-item">
                        <a href="mybookings.php" class="nav-link" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> My Bookings</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <div class="container">
        <div class="col-md-12">
            <h2 class="text-center">About Us</h2>
            <p>Ta' Borg Restaurant offers an enjoyable dining experience where you can come with your family and friends and enjoy our delicous food.</p>
            <h3><u>Menu Information:</u></h3>
            <p>The restaurant offers a vast menu with different types of food, ranging from starters, such as Calamari Fritti, to main course dishes, such as Tagliata di Manzo (cooked to your liking). The food offered at our restaurant is catered to your particular needs. By this, we mean that if you are Gluten intolerant, lactose intolerant, or vegan, the staff will guide you in choosing a plate according to your requirements.</p>
            <h3><u>Kids Play Area:</u></h3>
            <p>In our restaurant, there is also a sizeable Kids Play Area, where you can be rest-assured that your children are enjoying themselves and kept quiet.</p>
            <h3><u>What do I need to do if I want to book a table at your restaurant?</u></h3>
            <p>Firstly, if you don't have an account, click on "Register" to register for an account. When you have successfully registered to our website, you can fill in the form that will ask you the date and time you want to reserve a table, how many people will you be and lastly, if you have any other comments you would like to add to your booking/</p>
            <h3><u>Where is our restaurant located at?</u></h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d285.6941151117701!2d14.468346761645961!3d35.89164888615832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smt!4v1540889659631" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <!-- Register Modal -->
        <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="registerLabel">Register for an account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="adduser.php">

                            <input type="text" id="fName" name="firstname" placeholder="First Name" class="form-control" /><br />

                            <input type="text" id="lName" name="lastname" placeholder="Last Name" class="form-control" /><br />

                            <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                            <input type="text" placeholder="Password" class="form-control" name="password"><br>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <input type="submit" class="btn btn-primary" value="Register">
                    </div>
                </div>
            </div>
        </div>

        <!-- Login Modal -->
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginLabel">Login to your account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="viewuser.php">
                            <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                            <input type="text" placeholder="Password" class="form-control" name="password"><br>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <input type="submit" class="btn btn-secondary" value="Login">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
<?php
}
?>