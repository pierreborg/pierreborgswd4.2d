<?php
    ob_start();
    session_start();
    if(isset($_SESSION['Username'])){
?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!--FontAwesome CSS-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <title>Ta' Borg Restaurant</title>
    </head>

    <body class="bg-info">

        <div class="container-fluid bg-warning">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#">Ta' Borg Restaurant</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="aboutUs.php"><i class="fas fa-info-circle"></i> About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="menu.php"><i class="fas fa-file-alt"></i> View Menu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contactUs.php"><i class="fas fa-envelope"></i> Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#registerModal"><i class="fas fa-user-plus"></i> Register</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-toggle="modal" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> Login</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="container">
            <div class="col-md-12">
                <h2 class="text-center">Welcome to Ta' Borg Restaurant</h2>
            </div>

            <!-- Register Modal -->
            <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="registerLabel">Register for an account</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="adduser.php">

                                <input type="text" id="fName" name="firstname" placeholder="First Name" class="form-control" /><br />

                                <input type="text" id="lName" name="lastname" placeholder="Last Name" class="form-control" /><br />

                                <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                                <input type="text" placeholder="Password" class="form-control" name="password"><br>

                                <div class="modal-footer justify-content-center">
                                    <input type="submit" class="btn btn-primary" value="Register">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Login Modal -->
            <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="loginLabel">Login to your account</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="Login.php">
                                <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                                <input type="Password" placeholder="Password" class="form-control" name="password"><br>
                                <a href="forgot.php">
                                    <p>Forgot Password?</p>
                                </a>
                                <div class="modal-footer justify-content-center">
                                    <input type="submit" class="btn btn-secondary" value="Login">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
            if(isset($_POST['special'])){
                include_once ("DBLibrary.php");
                $Db = new DB_interaction;
                $con = $Db->connection("Localhost","root","");
                $special = htmlentities(mysqli_real_escape_string($con, $_POST['special']));
                $Db->insert("INSERT INTO specials (Specials) VALUES ('".$special."');",$con);
               header("location: menu.php");
                ob_end_flush();
            }
            else{
    ?>
            <form method="post" action="addSpecial.php">
                <p>Special</p>
                <input type="text" name="special">
                <input type="submit">
            </form>
           <?php
           }
    }else{
        header("location: index.php");
    }
        ?>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
