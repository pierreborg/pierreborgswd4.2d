<?php
class DB_interaction
{
#connects to database
public function connection($Server, $Username, $Password)
{
    $con = mysqli_connect($Server, $Username, $Password, "restaurant_db");
    if ($con->connect_error) {#returnError
        die("Connection failed: " . $con->connect_error);
    } 
    else{
        return $con;
    }

}
#updatest table 
public function insert($Sql, $con)
{
        $query = mysqli_query($con,$Sql);#sending query
}

#returns data in 1 row
public function returnRow($Sql, $con)
{
        $queryResult = mysqli_query($con,$Sql);#sending query
        $row = mysqli_fetch_row($queryResult);
        return $row;#returns data in row
}

#returns data of multiple rows
public function returnMultiRow($Sql, $con)
{
    if (!mysqli_query($con,$Sql))
    {
        return ("Error description: " . mysqli_error($con)); #returnError
    }
    else
    {
        $queryResult = mysqli_query($con,$Sql);#sending query
        $allRows = array();# initializing contaner of arrays
        while ($row=mysqli_fetch_row($queryResult)) {
            #inserting all results in another array I.E. array[0][1] this will make it easly accessible
            array_push($allRows,$row);
        }
        return $allRows;#returns data in rows
    }
}
}
?>