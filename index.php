<?php 
session_start();
if(!isset($_SESSION['Username'])){
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--FontAwesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Ta' Borg Restaurant</title>
</head>

<body class="bg-info">

    <div class="container-fluid bg-warning">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">Ta' Borg Restaurant</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="aboutUs.php"><i class="fas fa-info-circle"></i> About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="menu.php"><i class="fas fa-file-alt"></i> View Menu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactUs.php"><i class="fas fa-envelope"></i> Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#registerModal"><i class="fas fa-user-plus"></i> Register</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <div class="container">
        <div class="col-md-12">
            <h2 class="text-center">Welcome to Ta' Borg Restaurant</h2>

        </div>

        <!-- Register Modal -->
        <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="registerLabel">Register for an account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="adduser.php">

                            <input type="text" id="fName" name="firstname" placeholder="First Name" class="form-control" /><br />

                            <input type="text" id="lName" name="lastname" placeholder="Last Name" class="form-control" /><br />

                            <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                            <input type="text" placeholder="Password" class="form-control" name="password"><br>

                            <div class="modal-footer justify-content-center">
                                <input type="submit" class="btn btn-primary" value="Register">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Login Modal -->
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginLabel">Login to your account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="Login.php">
                            <input type="email" id="email" name="email" placeholder="Email" class="form-control" /><br />

                            <input type="Password" placeholder="Password" class="form-control" name="password"><br>
                            <a href="forgot.php">
                                <p>Forgot Password?</p>
                            </a>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <input type="submit" class="btn btn-secondary" value="Login">
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
<?php
}
else{

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--FontAwesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title>Ta' Borg Restaurant</title>
</head>

<body class="bg-info">

    <div class="container-fluid bg-warning">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">Ta' Borg Restaurant</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="aboutUs.php"><i class="fas fa-info-circle"></i> About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="menu.php"><i class="fas fa-file-alt"></i> View Menu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contactUs.php"><i class="fas fa-envelope"></i> Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="Logout.php" class="nav-link" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> Logout</a>
                    </li>
                    <li class="nav-item">
                        <a href="mybookings.php" class="nav-link" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> My Bookings</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <div class="container">
        <div class="col-md-12">
            <h2 class="text-center">Welcome to Ta' Borg Restaurant</h2>
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
            <link rel="stylesheet" href="/resources/demos/style.css">
            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script>
                $(function() {
                    var BookingDate = $('#datepicker').datepicker({
                        dateFormat: 'yy-mm-dd'
                    }).val();
                });

            </script>
            <?php
            $Sql = "Select Title, ID from users where Username ='".$_SESSION['Username']."';";
            include_once ("DBLibrary.php");
            $DB = new DB_interaction;
            $con = $DB->connection("Localhost","root","");
            $SqlGetbookings ="";
            $queryResult = $DB->returnRow($Sql,$con);
            if ($queryResult[0] == "a"){
            ?>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Specials</th>
                        <th scope="col">Delete</th>
                        <th scope="col">Update</th>
                    </tr>
                    <?php
                    
           
                    $getspecials = $DB->returnMultiRow('Select specials from specials',$con);
                        for($i=0;$i< count($getspecials);$i++){
                             echo "<tr>";
                            for($x=0;$x<count($getspecials[$i]);$x++){
                                echo "<td>".$getspecials[$i][$x] ."</td>";
                            }
                        echo "<td><a href='delete.php?name=".$getspecials[$i][0].'-0'."' <button type='button' class='btn btn-danger'>Delete</button></td>";
                        echo "<td><a href='updateSpecial.php?name=".$getspecials[$i][0]."' <button type='button' class='btn btn-primary'>Update</button></td>";
                        echo "</tr>";
                        }

                    echo ' </thead>';
                    echo '</table>';
                    echo '<a href="addSpecial.php"><button class="btn btn-success">Add special</button> </a>';
        }
        ?>
                    <br><br>
                    <form method="get" action="index.php">
                        <p>Date: <input type="text" name="BookingDate" id="datepicker"></p>
                        <input type="submit" class='btn btn-success'>
                    </form>

        </div>
        <?php
           if(isset($_GET['BookingDate'])){
             echo  '<table class="table">';
             echo  '<thead class="thead-dark">';
             echo '<tr>';
             echo    '<th scope="col">Table</th>';
            
            include_once ('DBLibrary.php');
            $DB = new DB_interaction;
            $con = $DB->connection("Localhost","root","");
            $Sqltimeslot = "Select * from timeslot";
            $getavailability = $DB->returnMultiRow($Sqltimeslot, $con);
            $Sqltable = "Select TableID , Seating From tables;";
            $gettables = $DB->returnMultiRow($Sqltable, $con);
            $Sqlbooking = "Select BookingDate, TimeSlotID, TableID from booking where BookingDate = '".$_GET['BookingDate']."';";
            $getbookings = $DB->returnMultiRow($Sqlbooking, $con);

   
            for ($i = 0; $i < count($getavailability);$i++) {
              echo '<th scope="col">'.$getavailability[$i][1].'</th>';
             } 
    
        ?>
        </tr>
        </thead>
        <tbody>
            <?php
              $preventFromWriting = 0;
              $preventFromWritingTaken = 0;
              if(count($getbookings) == 0){
                for ($i = 0; $i < count($gettables);$i++) {
                  echo '<tr> <td>'.$gettables[$i][0].'</td> ';
                  for($x = 0; $x < count($getavailability); $x++){
                    echo '<td><a href="book.php?name='.$x.'-'.$i.'-'.$_GET['BookingDate'].'"> <button type="button" class="btn btn-success">Available</button></a></td>';
                  } 
                }
              }
            else{
              for ($i = 0; $i < count($gettables);$i++) {
                echo '<tr> <td>'.$gettables[$i][0].'</td> ';
                for($x = 0; $x < count($getavailability); $x++){
                  for($y=0; $y < count($getbookings); $y++){
                   if($x +1 == $getbookings[$y][1] and $i+1 == $getbookings[$y][2]){
                    echo '<td><button type="button" class="btn btn-danger">Unavailable</button></td>';
                    $preventFromWritingTaken = 1;
                   }
                   else
                   {
                    $preventFromWriting=1;
                   }
                  }
                  if($preventFromWriting == 1 and $preventFromWritingTaken == 0){
                  echo '<td><a href="book.php?name='.$x.'-'.$i.'-'.$getbookings[0][0].'"> <button type="button" class="btn btn-success">Available</button></a></td>';
                  $preventFromWriting =0;

                  }
                  $preventFromWritingTaken = 0;
                  }
               }

            } 
          ?>
        </tbody>
        </table>

    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
<?php
           }
}
?>
