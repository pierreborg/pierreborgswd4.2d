<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!--FontAwesome CSS-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <title>Ta' Borg Restaurant</title>
    </head>

    <body class="bg-light">
        <div class="container">
            <div class="row mt-4">
                <div class="col-md-6 offset-md-3 text-center">

                <?php
                include_once ("DBLibrary.php");
                session_start();
                if(!isset($_SESSION['Username'])){
                    header("location: index.php");
                }
                else if(!isset($_POST['guests'])){
                    $rawdata = explode('-',$_GET['name'],3);
                    $get = $_GET['name'];

                ?>
                <form method="post" action="book.php?name=<?php echo $get;?>">
                    <div class="form-group">
                        <label for="guests">Number of guests</label>
                        <input type="text" name="guests" class="form-control" id="guests" placeholder="Enter number of guests">
                    </div>
                    <div class="form-group">
                        <label for="comment">Comments</label>
                        <textarea type="text" name="comment" class="form-control" rows="5" id="comment" placeholder="Extra Comments"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <?php
                }
                else{
                    function insertfunction($guest,$comment){
                        $SqlGetUser = "Select * from users where Username = '".$_SESSION['Username']."';";
                        $DB = new DB_interaction;
                        $con = $DB->connection("Localhost","root","");
                        $GetUser = $DB->returnRow($SqlGetUser,$con);
                        $rawdata = explode('-',$_GET['name'],3);
                        $timeslot = $rawdata[0] +1;
                        $table = $rawdata[1] +1;
                        $BookingDate = $rawdata[2];
                        $InsertSql = "INSERT INTO booking (TimeSlotID, Guests, Comments, TableID, UserID, BookingDate) values (".$timeslot.",".$guest.",'".$comment."',".$table.",".$GetUser[0].","."'".$BookingDate."'".");";
                        $DB->insert($InsertSql, $con);
                        mysqli_close($con);
                        $location = "location: index.php?BookingDate=".$BookingDate;
                        header($location);
                    }
                    if (!isset($_POST["comment"])){
                        $_POST['comment'] = " ";
                    }
                    insertfunction($_POST["guests"],$_POST["comment"]);
                }
                    ?>
                    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                </div>
            </div>
        </div>
    </body>
</html>
