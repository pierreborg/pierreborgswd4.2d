<?php
    session_start();
    if(isset($_SESSION['Username'])){
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!--FontAwesome CSS-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        
        <title>Ta' Borg Restaurant</title>
    </head>
    <body class="bg-info">

       <div class="container-fluid bg-warning">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand" href="#">Ta' Borg Restaurant</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="index.php"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="aboutUs.php"><i class="fas fa-info-circle"></i> About Us</a>
						</li>
                        <li class="nav-item">
                            <a class="nav-link" href="menu.php"><i class="fas fa-file-alt"></i> View Menu</a>
                        </li>
						<li class="nav-item">
							<a class="nav-link" href="contactUs.php"><i class="fas fa-envelope"></i> Contact Us</a>
						</li>
						<li class="nav-item">
						    <a href="Logout.php" class="nav-link" data-target="#loginModal"><i class="fas fa-sign-in-alt"></i> Logout</a>
						</li>
					</ul>
				</div>
			</nav>
        </div>
           
            
        <div class="container">
           <div class="col-md-12">
               <h2 class="text-center">List of Bookings</h2>
               
           </div>
           <?php
           $Sql = "Select Title, ID from users where Username ='".$_SESSION['Username']."';";
           include_once ("DBLibrary.php");
           $DB = new DB_interaction;
           $con = $DB->connection("Localhost","root","");
           $SqlGetbookings ="";
           $queryResult = $DB->returnRow($Sql,$con);

            ?>
           
           <table class="table">
                    <thead class="thead-dark">
                        <tr>
                         <th scope="col">Booking ID</th>
                         <th scope="col">Time Slot</th>
                         <th scope="col">Guests</th>
                         <th scope="col">Comments</th>
                         <th scope="col">Created</th>
                         <th scope="col">TableID</th>
                         <th scope="col">UserID</th>
                         <th scope="col">Booking Date</th>
                         <th scope="col">Delete</th>
                         <th scope="col">Update</th>
                        </tr>
                    </thead>
              <?php
              
              if($queryResult[0] == "a"){
                    $SqlGetbookings = "SELECT BookingID,timeslot,Guests,Comments,Created,TableID,UserID,BookingDate FROM booking inner join timeslot on booking.TimeSlotID = timeslot.TimeSlotID;";
                    

              }
              else{
                    $SqlGetbookings = "SELECT BookingID,timeslot,Guests,Comments,Created,TableID,UserID,BookingDate FROM booking inner join timeslot on booking.TimeSlotID = timeslot.TimeSlotID where UserID=".$queryResult[1];
              }
              $getbookings = $DB->returnMultiRow($SqlGetbookings,$con);
              for($i=0;$i< count($getbookings);$i++){
                echo "<tr>";
                for($x=0;$x<count($getbookings[$i]);$x++){
                    echo "<td>".$getbookings[$i][$x] ."</td>";
                }
                echo "<td><a href='delete.php?name=".$getbookings[$i][0].'-1'."'<button type='button' class='btn btn-danger'>Delete</button></td>";
                echo "<td><a href='updateDate.php?name=".$getbookings[$i][0].'-1'."'<button type='button' class='btn btn-primary'>Update</button></td>";
                echo "</tr>";
                
              }
                    
              ?>
            </table>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
<?php
    }
    else{
        header("location: index.php");
    }
?>